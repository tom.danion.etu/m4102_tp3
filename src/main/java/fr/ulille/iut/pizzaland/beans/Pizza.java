package fr.ulille.iut.pizzaland.beans;

import java.util.List;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private long id;
	private String name;
	private List<Ingredient> ingredients;
	
	public Pizza() {}
	
	public Pizza(long id, List<Ingredient> ingredients, String name) {
		this.id = id;
		this.name = name;
		this.ingredients = ingredients;
	}
	
	public static PizzaCreateDto toCreateDto(Pizza pizza) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setIngredients(pizza.getIngredients());

		return dto;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		Pizza pizza = new Pizza();
		pizza.setIngredients(dto.getIngredients());

		return pizza;
	}
	
	public long getId() {
	    return id;
	}

	public void setId(long id) {
	    this.id = id;
	}
	
	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients=ingredients;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public static PizzaDto toDto(Pizza i) {
		PizzaDto dto = new PizzaDto();
		dto.setId(i.getId());
		dto.setIngredients(i.getIngredients());
		dto.setName(i.getName());

		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setId(dto.getId());
		pizza.setIngredients(dto.getIngredients());
		pizza.setName(dto.getName());

		return pizza;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id != other.id)
			return false;
		if (ingredients == null) {
			if (other.ingredients != null)
				return false;
		} else if (!ingredients.equals(other.ingredients))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pizza [id=" + id + ", name="+ name +", ingredients=" + ingredients.toString() + "]";
	}
}
